package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class PhotoInfo {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "imageviewer.testdon.imagebrowse");
        schema.enableKeepSectionsByDefault();
        createDataModel(schema);
        new DaoGenerator().generateAll(schema, "../app/src/main/java");

    }

    private static void createDataModel(Schema schema) {
        Entity album = schema.addEntity("Pic");
        Property id = album.addStringProperty("albumId").getProperty();
        album.addStringProperty("id");
        album.addStringProperty("title");
        album.addStringProperty("url");
        album.addStringProperty("thumbnailUrl");
    }
}
