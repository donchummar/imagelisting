package imageviewer.testdon.imagebrowse.app;

import android.app.Application;

import imageviewer.testdon.imagebrowse.generatedmodel.DataBasemanager;

/**
 * Created by Don Chummar on 8/13/2015.
 */
public class BrowserApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        intVolley();
        initGreenDao();
    }

    private void initGreenDao() {
        new DataBasemanager(this);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        DataBasemanager.getInstance(this).closeConnection();
    }

    private void intVolley() {
        MyVolley.init(this);
    }
}
