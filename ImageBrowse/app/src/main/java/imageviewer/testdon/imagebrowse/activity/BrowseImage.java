package imageviewer.testdon.imagebrowse.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import imageviewer.testdon.imagebrowse.R;
import imageviewer.testdon.imagebrowse.adaptor.ImageGridAdaptor;
import imageviewer.testdon.imagebrowse.app.MyVolley;
import imageviewer.testdon.imagebrowse.generatedmodel.DataBasemanager;
import imageviewer.testdon.imagebrowse.generatedmodel.IBaseModel;
import imageviewer.testdon.imagebrowse.generatedmodel.ImageDetailsList;
import imageviewer.testdon.imagebrowse.generatedmodel.Pic;
import imageviewer.testdon.imagebrowse.net.GsonRequest;
import imageviewer.testdon.imagebrowse.utils.AppUtils;
import imageviewer.testdon.imagebrowse.utils.Constant;
import imageviewer.testdon.imagebrowse.utils.GridSpacingItemDecoration;

public class BrowseImage extends AppCompatActivity implements Response.Listener<IBaseModel>, Response.ErrorListener {

    private GsonRequest mRequest;
    private RecyclerView mImageListView;
    private ImageGridAdaptor mAdapter;
    private View mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_image);

        initViews();
        loadData();
    }

    private void initViews() {
        mImageListView = (RecyclerView) findViewById(R.id.image_list);
        mProgressView = findViewById(R.id.progress_load);
        mImageListView.setHasFixedSize(true);
        mImageListView.setLayoutManager(new GridLayoutManager(this, 2));
        int spanCount = 2;
        int spacing = 50;
        boolean includeEdge = false;
        mImageListView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
    }

    private void loadData() {
        ArrayList<Pic> imageList = (ArrayList<Pic>) DataBasemanager.getInstance(this).getAlbumData();
        if (imageList != null && imageList.size() > 0) {
            displayImages(imageList);
        } else {
            if (AppUtils.isNetworkAvailable(this)) {
                loadFromServer();
            } else {
                Toast.makeText(this, "Please connect to network", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadFromServer() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        String url = Constant.HOME_URL;
        mRequest = new GsonRequest(url, new ImageDetailsList(), this, this);
        MyVolley.getRequestQueue().add(mRequest);

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(IBaseModel response) {

        if (response instanceof ImageDetailsList) {
            ImageDetailsList imageDetails = (ImageDetailsList) response;
            Log.d("tag", imageDetails.toString());
            DataBasemanager.getInstance(this).setAlbumData(imageDetails.getImageList());
            displayImages(imageDetails.getImageList());
        }
    }

    private void displayImages(ArrayList<Pic> imageDetails) {
        mProgressView.setVisibility(View.GONE);
        mImageListView.setVisibility(View.VISIBLE);
        if (mAdapter == null) {
            mAdapter = new ImageGridAdaptor(this,imageDetails,mImageListView);
            mImageListView.setAdapter(mAdapter);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mRequest != null) {
            mRequest.cancel();
            mRequest = null;
        }
    }
}
