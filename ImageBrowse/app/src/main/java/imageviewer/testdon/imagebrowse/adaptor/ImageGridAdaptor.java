package imageviewer.testdon.imagebrowse.adaptor;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.io.Serializable;
import java.util.List;

import imageviewer.testdon.imagebrowse.R;
import imageviewer.testdon.imagebrowse.activity.ImageDetail;
import imageviewer.testdon.imagebrowse.generatedmodel.Pic;
import imageviewer.testdon.imagebrowse.utils.Constant;
import imageviewer.testdon.imagebrowse.utils.ImageCacheManager;

/**
 * Created by Don Chummar on 8/13/2015.
 */
public class ImageGridAdaptor extends RecyclerView.Adapter<ImageGridAdaptor.ViewHolder> {

    private final RecyclerView mImageListView;
    private final Context mContext;
    private List<Pic> mImageList;
    private ImageLoader mImageLoader;

    public ImageGridAdaptor(Context context, List<Pic> imageList, RecyclerView imageListView) {
        this.mImageList = imageList;
        mImageListView = imageListView;
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        String url = mImageList.get(i).getThumbnailUrl();
        final ImageView loadingImage = viewHolder.imageItem;
        final View progress = viewHolder.progress;
        mImageLoader = ImageCacheManager.INSTANCE.getImageLoader();
        mImageLoader.get(url, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response != null) {
                    Bitmap bitmap = response.getBitmap();
                    if (bitmap != null) {
                        progress.setVisibility(View.GONE);
                        loadingImage.setVisibility(View.VISIBLE);
                        loadingImage.setImageBitmap(bitmap);
                    }
                }
            }

        });

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.grid_item, viewGroup, false);
        itemView.setOnClickListener(clickListener);

        return new ViewHolder(itemView);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageItem;
        protected View progress;
        public ViewHolder(View v) {
            super(v);
            imageItem = (ImageView) v.findViewById(R.id.image_item);
            progress =  v.findViewById(R.id.progress_load);

        }
    }

     View.OnClickListener clickListener = new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             int pos = mImageListView.getChildLayoutPosition(view);
             Pic data = mImageList.get(pos);
             startDetailPage(data);
         }
     };

    private void startDetailPage(Pic data) {
        Intent intent = new Intent(mContext, ImageDetail.class);
        intent.putExtra(Constant.IMAGE_URL, data.getUrl());
        mContext.startActivity(intent);
    }
}

