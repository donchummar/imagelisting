package imageviewer.testdon.imagebrowse.activity;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import imageviewer.testdon.imagebrowse.R;
import imageviewer.testdon.imagebrowse.utils.Constant;
import imageviewer.testdon.imagebrowse.utils.ImageCacheManager;

public class ImageDetail extends AppCompatActivity {

    private ImageView mImageView;
    private View mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);
        mImageView = (ImageView) findViewById(R.id.detail_image);
        mProgress =  findViewById(R.id.progress_load);
        dispayImage();

    }

    private void dispayImage() {
        if (getIntent().hasExtra(Constant.IMAGE_URL)) {
            String imgaeurl = getIntent().getStringExtra(Constant.IMAGE_URL);
            ImageLoader imageLoader = ImageCacheManager.INSTANCE.getImageLoader();
            imageLoader.get(imgaeurl, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError arg0) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response != null) {
                        Bitmap bitmap = response.getBitmap();
                        if (bitmap != null) {
                            mProgress.setVisibility(View.GONE);
                            mImageView.setVisibility(View.VISIBLE);
                            mImageView.setImageBitmap(bitmap);
                        }
                    }
                }

            });
        }
    }

}
