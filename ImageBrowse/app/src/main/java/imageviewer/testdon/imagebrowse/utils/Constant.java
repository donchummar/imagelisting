package imageviewer.testdon.imagebrowse.utils;

/**
 * Created by Don Chummar on 8/13/2015.
 */
public class Constant {
    public static final String  HOME_URL = "http://jsonplaceholder.typicode.com/photos";
    public static final String IMAGE_URL = "image_url";
}
