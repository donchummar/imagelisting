package imageviewer.testdon.imagebrowse.generatedmodel;

import java.util.ArrayList;

/**
 * Created by Don Chummar on 8/13/2015.
 */
public class ImageDetailsList implements IBaseModel{

    private ArrayList<Pic> mImageList = new ArrayList<>();

    public void setmImageList(ArrayList<Pic> mImageList) {
        this.mImageList = mImageList;
    }

    public ArrayList<Pic> getImageList() {
        return mImageList;
    }
}
