package imageviewer.testdon.imagebrowse.generatedmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Don Chummar on 8/13/2015.
 */
public class ImageDetails  implements IBaseModel{
    @SerializedName("id")
    private String mId;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("thumbnailUrl")
    private String mSmallImage;
    @SerializedName("url")
    private String mBigImage;

    public String getmId() {
        return mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmSmallImage() {
        return mSmallImage;
    }

    public String getmBigImage() {
        return mBigImage;
    }
}
