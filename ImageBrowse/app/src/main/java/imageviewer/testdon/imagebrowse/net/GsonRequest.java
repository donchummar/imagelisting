package imageviewer.testdon.imagebrowse.net;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import imageviewer.testdon.imagebrowse.generatedmodel.IBaseModel;
import imageviewer.testdon.imagebrowse.generatedmodel.ImageDetailsList;
import imageviewer.testdon.imagebrowse.generatedmodel.Pic;

/**
 * Created by Don Chummar on 8/13/2015.
 */
public class GsonRequest<T> extends Request<IBaseModel> {
    private final Gson gson = new Gson();
    private IBaseModel dataModel;
    private Map<String, String> headers;
    private final Response.Listener<IBaseModel> listener;
    public static final int MY_SOCKET_TIMEOUT_MS = 30000;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url       URL of the request to make
     * @param dataModel Relevant class object, for Gson's reflection
     * @param headers   Map of request headers
     */
    public GsonRequest(String url, IBaseModel dataModel, Map<String, String> headers,
                       Response.Listener<IBaseModel> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.dataModel = dataModel;
        this.headers = headers;
        this.listener = listener;
    }

    public GsonRequest(String url, IBaseModel dataModel, Response.Listener<IBaseModel> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = listener;
        this.dataModel = dataModel;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected Response<IBaseModel> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            if (dataModel instanceof ImageDetailsList) {
                Pic[] orderList = gson.fromJson(json, Pic[].class);
                if (orderList != null) {
                    ImageDetailsList list = new ImageDetailsList();
                    list.setmImageList(new ArrayList<Pic>(Arrays.asList(orderList)));
                    return Response.success((IBaseModel) list, getCacheEntry());
                }
                return Response.success(dataModel, getCacheEntry());
            }
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
        return null;
    }


    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return super.setRetryPolicy(retryPolicy);
    }

    @Override
    protected void deliverResponse(IBaseModel response) {
        listener.onResponse(response);
    }

}
