
package imageviewer.testdon.imagebrowse.app;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import imageviewer.testdon.imagebrowse.utils.ImageCacheManager;

public class MyVolley {
	
	private static RequestQueue mRequestQueue;
	private static RequestQueue mImageRequestQueue;
	
	
	private MyVolley() {}
	
	/**
	 * initialize Volley
	 */
	public static void init(Context context) {
		mRequestQueue = Volley.newRequestQueue(context,false);
		mImageRequestQueue = Volley.newRequestQueue(context, true);
		ImageCacheManager.INSTANCE.initImageCache();
	}
	
	public static RequestQueue getRequestQueue() {
		if (mRequestQueue != null) {
			return mRequestQueue;
		} else {
			throw new IllegalStateException("RequestQueue not initialized");
		}
	}
	
	public static RequestQueue getImageRequestQueue() {

		if (mImageRequestQueue != null) {
			return mImageRequestQueue;
		} else {
			throw new IllegalStateException("Image RequestQueue not initialized");
		}
	}
}
