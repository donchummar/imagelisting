package imageviewer.testdon.imagebrowse.generatedmodel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

/**
 * Created by Don Chummar on 8/14/2015.
 */
public class DataBasemanager {

    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase mDataBase;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;
    private static DataBasemanager mInstance;
    private static final String DATA_BASE_NAME = "compass.db";

    public DataBasemanager(Context context) {
        initDataBase(context);
        mInstance = this;
    }

    private void initDataBase(Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context, DATA_BASE_NAME, null);
        mDataBase = mHelper.getWritableDatabase();
        mDaoMaster = new DaoMaster(mDataBase);
        mDaoSession = mDaoMaster.newSession();
    }

    public void closeConnection() {
        if (mHelper != null) {
            mHelper.close();
        }
    }

    public static DataBasemanager getInstance(Context context) {
        if (mInstance == null) {
            synchronized (DataBasemanager.class) {
                if (mInstance == null) {
                    mInstance = new DataBasemanager(context.getApplicationContext());
                }
            }
        }
        return mInstance;
    }

    public List<Pic> getAlbumData() {
        PicDao AlbumDao = mDaoSession.getPicDao();
        return AlbumDao.loadAll();
    }

    public void setAlbumData(List<Pic> list) {
        PicDao AlbumDao = mDaoSession.getPicDao();
        AlbumDao.insertInTx(list);

    }

}

